# env-test

## Project setup

```
npm install 下载依赖文件
```

### Compiles and hot-reloads for development

```
1. 用``npm run serve``启动项目，发起开发环境（baseUrl为development.com）的网络请求

2. 用``npm run serve:pro``启动项目，发起生产环境(baseUrl为production.com)的网络请求

```

### Compiles and minifies for production

```
用``npm run build:pro``打包项目放在生产环境，如果是自动化部署，这里应该修改Dockerfile文件里的配置，因为打包后的项目放在了dist/build/h5文件夹下了
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
