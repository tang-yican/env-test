// 这个 NODE_ENV 就是环境变量
// 当用npm run serve启动项目时，NODE_ENV 为 development
// 当用npm run serve:pro启动项目时，NODE_ENV 为 production
const { NODE_ENV } = process.env;
const config = {
  development: {
    baseUrl: "https://development.com/api/",
  },
  production: {
    baseUrl: "https://production.com/api/",
  },
};

export default {
  baseUrl: config[NODE_ENV].baseUrl,
  shareUrl: config[NODE_ENV].shareUrl,
};
