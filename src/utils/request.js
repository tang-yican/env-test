// 引入环境配置变量文件
import config from "../config/index.js";
const baseUrl = config.baseUrl;

function request(url, data = {}, method = "POST") {
  const param = {
    ...data,
  };
  return new Promise((resolve, reject) => {
    uni
      .request({
        url: `${baseUrl}${url}`,
        header: {
          "content-type": "application/x-www-form-urlencoded",
        },
        data: param,
        method,
      })
      .then((res) => {
        const result = res[1].data;
        if (Number(result.code) === 200) {
          resolve(result.data);
        } else {
          reject(result);
        }
      })
      .catch((err) => {
        uni.showToast({
          title: "数据请求失败",
          icon: "none",
          duration: 2000,
        });
      });
  });
}

export default request;
